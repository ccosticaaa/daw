﻿using System.Collections.Generic;

namespace DAWApp.Models
{
    public class Subscription
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int TopicId { get; set; }
    }
}