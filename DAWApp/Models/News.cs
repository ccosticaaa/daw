﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DAWApp.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public int TopicId { get; set; }
        public ICollection<ReadNews> ReadNews { get; set; } 

    }
}