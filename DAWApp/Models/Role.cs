﻿using DAWApp.App;

namespace DAWApp.Models
{
    public static class Role
    {
        public const string Admin = "Admin"; // 1 
        public const string User = "User"; // 
        public const string Guest = "Guest";

        public const string AdminOrGuest = Admin + "," + User;
    }
    
    
}