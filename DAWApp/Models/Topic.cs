﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DAWApp.Models
{
    public class Topic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public ICollection<News> News { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }
    }
}