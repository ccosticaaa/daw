﻿namespace DAWApp.Models
{
    public class ReadNews
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int NewsId { get; set; }
    }
}