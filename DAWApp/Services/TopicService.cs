﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DAWApp.App;
using DAWApp.Mappers.Topics;
using DAWApp.Models;
using DAWApp.Validators.Topic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DAWApp.Services
{
    public interface ITopicService
    {
        IEnumerable<Topic> GetAll();
        IEnumerable<Topic> GetAllWithNews();
        IEnumerable<Topic> GetAllWithSubscriptionsByUser(int userId);
        IEnumerable<Topic> GetTopicsByUserSubscription(int userId);
        Topic GetById(int id);
        Topic Create(Topic topic);
        void Update(Topic user);
        void Delete(int id);
    }

    public class TopicService : ITopicService
    {
        private Database dbConn;

        public TopicService(Database db)
        {
            dbConn = db;
        }

        public IEnumerable<Topic> GetAll()
        {
            return dbConn.Topics;
        }

        public IEnumerable<Topic> GetAllWithNews()
        {
            return dbConn.Topics.Include(t => t.News);
        }

        public IEnumerable<Topic> GetAllWithSubscriptionsByUser(int userId)
        {
            return dbConn.Topics.Include(t => t.Subscriptions);
//            return dbConn.Topics.Include(t => t.Subscriptions)
//                .Include(u => u.Subscriptions.Select(u => u.UserId == ));
        }

        public IEnumerable<Topic> GetTopicsByUserSubscription(int userId)
        {
            return dbConn.Topics.Where(t => t.Subscriptions
                .Any(s => s.UserId == userId));
//            return dbConn.Topics.Include(t => t.Subscriptions);
        }

        public Topic GetById(int id)
        {
            var topic = dbConn.Topics.Include(t => t.News)
                .SingleOrDefault(x => x.Id == id);
            
            if (topic == null)
                throw new AppException("Topic not found");

            return topic;
        }

        public Topic Create(Topic topic)
        {
            new TopicValidator(dbConn, topic);

            dbConn.Topics.Add(topic);
            dbConn.SaveChanges();

            return topic;
        }

        public void Update(Topic topicParam)
        {
            var topic = dbConn.Topics.Find(topicParam.Id);

            if (topic == null)
                throw new AppException("Topic not found");

            // update name if it has changed
            if (!string.IsNullOrWhiteSpace(topicParam.Name) && topicParam.Name != topic.Name)
            {
                // throw error if the new email is already taken
                if (dbConn.Topics.Any(x => x.Name == topicParam.Name))
                    throw new AppException("Topic " + topicParam.Name + " is already taken");

                topic.Name = topicParam.Name;
            }

            dbConn.Topics.Update(topic);
            dbConn.SaveChanges();
        }

        public void Delete(int id)
        {
            var topic = dbConn.Topics.Find(id);
            
            if (topic == null)
                throw new AppException("Topic not found");
            
            dbConn.Topics.Remove(topic);
            dbConn.SaveChanges();
        }
    }
}