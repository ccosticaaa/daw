﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DAWApp.App;
using DAWApp.Models;
using DAWApp.Validators.News;

namespace DAWApp.Services
{
    public interface INewsService
    {
        IEnumerable<News> GetAll();
        News GetById(int id);
        IEnumerable<News> GetByTopicId(int id);
        News Create(News news);
        void Update(News user);
        void Delete(int id);
    }

    public class NewsService : INewsService
    {
        private Database dbConn;

        public NewsService(Database db)
        {
            dbConn = db;
        }

        public IEnumerable<News> GetAll()
        {
            return dbConn.News;
        }

        public News GetById(int id)
        {
            var news = dbConn.News.Find(id);
            
            if (news == null)
                throw new AppException("News not found");

            return news;
        }

        public IEnumerable<News> GetByTopicId(int id)
        {
            var news = dbConn.News.Where(x => x.TopicId == id);
            
            if (news == null)
                throw new AppException("News not found");

            return news;
        }
        
        public News Create(News news)
        {
            new NewsCreateValidator(dbConn, news);

            dbConn.News.Add(news);
            dbConn.SaveChanges();

            return news;
        }

        public void Update(News news)
        {
            if (news == null)
            {
                throw new AppException("News not found. Invalid id");
            }

            new NewsUpdateValidator(dbConn, news);

            dbConn.News.Update(news);
            dbConn.SaveChanges();
        }

        public void Delete(int id)
        {
            var news = dbConn.News.Find(id);
            
            if (news == null)
                throw new AppException("News not found");
            
            dbConn.News.Remove(news);
            dbConn.SaveChanges();
        }
    }
}