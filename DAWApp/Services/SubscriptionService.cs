﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DAWApp.App;
using DAWApp.Mappers.Subscriptions;
using DAWApp.Models;
using DAWApp.Validators.Subscription;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DAWApp.Services
{
    public interface ISubscriptionService
    {
        IEnumerable<Subscription> GetAll();
//        IEnumerable<Subscription> GetAllWithNews();
//        Subscription GetById(int id);
        Subscription Create(Subscription subscription);
        Subscription CreateOrDelete(Subscription subscription);
        void Update(Subscription subscription);
        void Delete(int id);
    }

    public class SubscriptionService : ISubscriptionService
    {
        private Database dbConn;

        public SubscriptionService(Database db)
        {
            dbConn = db;
        }

        public IEnumerable<Subscription> GetAll()
        {
            return dbConn.Subscriptions;
        }

//        public IEnumerable<Subscription> GetAllWithNews()
//        {
//            return dbConn.Subscriptions.Include(t => t.News);
//        }
//
//        public Subscription GetById(int id)
//        {
//            var subscription = dbConn.Subscriptions.Include(t => t.News)
//                .SingleOrDefault(x => x.Id == id);
//            
//            if (subscription == null)
//                throw new AppException("Subscription not found");
//
//            return subscription;
//        }

        public Subscription CreateOrDelete(Subscription subscription)
        {
            Subscription response;
            
            var exists = dbConn.Subscriptions
                .FirstOrDefault(s => s.UserId == subscription.UserId && s.TopicId == subscription.TopicId);

            if (exists == null)
            {
                response = Create(subscription);
            }
            else
            {
                subscription.Id = exists.Id;
                Delete(subscription.Id);
                response = subscription;
            }

            return response;
        }
        
        public Subscription Create(Subscription subscription)
        {
            new SubscriptionValidator(dbConn, subscription);

            dbConn.Subscriptions.Add(subscription);
            dbConn.SaveChanges();

            return subscription;
        }

        public void Update(Subscription subscriptionParam)
        {
            var subscription = dbConn.Subscriptions.Find(subscriptionParam.Id);

            if (subscription == null)
                throw new AppException("Subscription not found");

            // throw error if the new email is already taken
//            if (dbConn.Subscriptions.Any(x => x.TopicId == subscriptionParam.TopicId && x.UserId == subscriptionParam.UserId))
//                throw new AppException("Subscription already exists");

            dbConn.Subscriptions.Update(subscription);
            dbConn.SaveChanges();
        }

        public void Delete(int id)
        {
            var subscription = dbConn.Subscriptions.Find(id);
            
            if (subscription == null)
                throw new AppException("Subscription not found");
            
            dbConn.Subscriptions.Remove(subscription);
            dbConn.SaveChanges();
        }
    }
}