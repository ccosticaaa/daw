﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text.Json;
using System.Threading.Tasks;
using DAWApp.App;
using DAWApp.Mappers.Reports;
using DAWApp.Models;
using DAWApp.Validators.ReadNews;
using Microsoft.EntityFrameworkCore;

namespace DAWApp.Services
{
    public interface IReportsService
    {
        
        Task<Object> GetMostClickedNews();
        Task<Object> GetMostSubscribedNews();
    }
    public class ReportsService : IReportsService
    {
        private Database dbConn;
        
        public ReportsService(Database db)
        {
            dbConn = db;
        }
        
        public async Task<Object> GetMostClickedNews()
        {
            var reportData = dbConn.News.Include(r => r.ReadNews)
                .OrderByDescending(u => u.ReadNews.Count)
                .Select(x => new {Name = x.Name, Total = x.ReadNews.Count})
                .Take(10);

            return new
            {
                data = reportData,
                jsonName = "GetMostClickedNews"
            };
        }

        public static MostClickedMapper ToMostClickedMapper(News news)
        {
            return new MostClickedMapper()
            {
                Name = news.Name,
                Total = news.ReadNews.Count,
            };
        }
        
        public async Task<Object> GetMostSubscribedNews()
        {
            var reportData = dbConn.Topics.Include(t => t.Subscriptions)
                .OrderByDescending(t => t.Subscriptions.Count)
                .Select(x => new {Name = x.Name, Total = x.Subscriptions.Count});

            return new
            {
                data = reportData,
                jsonName = "GetMostSubscribedNews"
            };
        }

    }
}