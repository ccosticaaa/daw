﻿using System.Collections.Generic;
using DAWApp.App;
using DAWApp.Models;
using DAWApp.Validators.ReadNews;

namespace DAWApp.Services
{
    
    public interface IReadNewsService
    {
        ReadNews Create(ReadNews news);
        void Delete(int id);
    }
    public class ReadNewsService : IReadNewsService
    {
        private Database dbConn;
        
        public ReadNewsService(Database db)
        {
            dbConn = db;
        }
        
        public ReadNews Create(ReadNews readNews)
        {
            new ReadNewsCreateValidator(dbConn, readNews);

            dbConn.ReadNews.Add(readNews);
            dbConn.SaveChanges();

            return readNews;
        }
        
        public void Delete(int id)
        {
            var readNews = dbConn.ReadNews.Find(id);
            
            if (readNews == null)
                throw new AppException("News not found");
            
            dbConn.ReadNews.Remove(readNews);
            dbConn.SaveChanges();
        }
    }
}