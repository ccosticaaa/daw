﻿namespace DAWApp.Validators
{
    public interface IValidator
    {
        void Validate();
    }
}