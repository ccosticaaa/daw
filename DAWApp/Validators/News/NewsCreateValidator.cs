﻿using System;
using System.Linq;
using DAWApp.App;

namespace DAWApp.Validators.News
{
    public class NewsCreateValidator : IValidator
    {
        private Database dbConn;
        private Models.News news;
        
        public NewsCreateValidator(Database dbConn, Models.News news)
        {
            this.dbConn = dbConn;
            this.news = news;
            
            this.Validate();
        }

        public void Validate()
        {
            // validation
            if (string.IsNullOrWhiteSpace(news.Name))
            {
                throw new AppException("Title can not be empty");
            }
            
            // validation
            if (string.IsNullOrWhiteSpace(news.Body))
            {
                throw new AppException("Content can not be empty");
            }
            
            
            if (dbConn.News.Any(x => x.Name == news.Name))
            {
                throw new AppException("News name " + news.Name + " is already taken");                
            }
            
            if (! dbConn.Topics.Any(x => x.Id == news.TopicId))
            {
                throw new AppException("Invalid topic selected");
            }
        }
    }
}