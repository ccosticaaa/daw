﻿using System.Linq;
using DAWApp.App;

namespace DAWApp.Validators.User
{
    public class RegisterValidator : IValidator
    {
        private Database dbConn;
        private string email;
        private string password;
        
        public RegisterValidator(Database dbConn, string email, string password)
        {
            this.dbConn = dbConn;
            this.email = email;
            this.password = password;
            
            this.Validate();
        }

        public void Validate()
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new AppException("Password is required");
            }

            if (dbConn.Users.Any(x => x.Email == email))
            {
                throw new AppException("Email " + email + " is already taken");                
            }
            
            if ( ! Helpers.isValidEmail(email)) {
                throw new AppException("Invalid email address");                
            }
        }
        
        
        
    }
}