﻿using DAWApp.App;
using System.Linq;

namespace DAWApp.Validators.ReadNews
{
    public class ReadNewsCreateValidator : IValidator
    {
        private Database dbConn;
        private Models.ReadNews readNews;
        
        public ReadNewsCreateValidator(Database dbConn, Models.ReadNews readNews)
        {
            this.dbConn = dbConn;
            this.readNews = readNews;
            
            Validate();
        }
        
        public void Validate()
        {
            if (dbConn.ReadNews.Any(x => x.UserId == readNews.UserId && x.NewsId == readNews.NewsId))
            {
                throw new AppException("User reading session of this article has already been registered");                
            }
        }
    }
}