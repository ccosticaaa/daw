﻿using System.Linq;
using DAWApp.App;

namespace DAWApp.Validators.Topic
{
    public class TopicValidator : IValidator
    {
        private Database dbConn;
        private Models.Topic topic;
        
        public TopicValidator(Database dbConn, Models.Topic topic)
        {
            this.dbConn = dbConn;
            this.topic = topic;
            
            this.Validate();
        }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(topic.Name))
            {
                throw new AppException("Name can not be empty");
            }
            if (dbConn.Topics.Any(x => x.Name == topic.Name))
            {
                throw new AppException("Topic name " + topic.Name + " is already taken");                
            }
        }
    }
}