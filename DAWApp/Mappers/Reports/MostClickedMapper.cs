﻿namespace DAWApp.Mappers.Reports
{
    public class MostClickedMapper
    {
        public string Name { get; set; }
        public int Total { get; set; }
    }
}