﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAWApp.Models;

namespace DAWApp.Mappers.Topics
{
    public class TopicMapper
    {
//        [Required]
        public int Id { get; set; }
        
//        [Required]
        public string Name { get; set; }
        
        public ICollection<Models.News> News { get; set; }
        
        public ICollection<Subscription> Subscriptions { get; set; }


    }
}