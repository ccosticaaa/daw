﻿using System.ComponentModel.DataAnnotations;

namespace DAWApp.Mappers.Topics
{
    public class CreateTopicMapper
    {
        public string Name { get; set; }
    }
}