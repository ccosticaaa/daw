﻿namespace DAWApp.Mappers.ReadNews
{
    public class ReadNewsMapper
    {
        public int Id { get; set; }
        
        public int UserId { get; set; }
        
        public int NewsId { get; set; }
    }
}