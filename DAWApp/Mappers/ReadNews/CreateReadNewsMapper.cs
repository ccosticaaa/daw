﻿namespace DAWApp.Mappers.ReadNews
{
    public class CreateReadNewsMapper
    {
        public int UserId { get; set; }
        
        public int NewsId { get; set; }
    }
}