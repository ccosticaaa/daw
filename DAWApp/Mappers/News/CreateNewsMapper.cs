﻿using System.ComponentModel.DataAnnotations;

namespace DAWApp.Mappers.News
{
    public class CreateNewsMapper
    {
        public string Name { get; set; }
        public string Body { get; set; }
        public int TopicId { get; set; }
    }
}