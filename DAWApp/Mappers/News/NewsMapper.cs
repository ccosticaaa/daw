﻿using System.Collections.Generic;
using DAWApp.Models;

namespace DAWApp.Mappers.News
{
    public class NewsMapper
    {
        //        [Required]
        public int Id { get; set; }
        
//        [Required]
        public string Name { get; set; }
        
//        [Required]
        public string Body { get; set; }
        
        //        [Required]
        public int TopicId { get; set; }
        
        public ICollection<Models.ReadNews> ReadNews { get; set; } 
    }
}