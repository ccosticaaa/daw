﻿namespace DAWApp.Mappers.News
{
    public class UpdateNewsMapper
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public int TopicId { get; set; }
    }
}