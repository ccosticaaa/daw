﻿using System.Collections.Generic;
using DAWApp.Models;

namespace DAWApp.Mappers.Users
{
  public class UserMapper
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        
        public ICollection<Subscription> Subscriptions { get; set; }
        public ICollection<Models.ReadNews> ReadNews { get; set; } 

    }
}