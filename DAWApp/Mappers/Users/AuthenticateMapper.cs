﻿using System.ComponentModel.DataAnnotations;

namespace DAWApp.Mappers.Users
{
    public class AuthenticateMapper
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}