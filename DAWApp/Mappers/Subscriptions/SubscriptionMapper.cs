﻿namespace DAWApp.Mappers.Subscriptions
{
    public class SubscriptionMapper
    {
        public int Id { get; set; }
        
        public int UserId { get; set; }
        
        public int TopicId { get; set; }
    }
}