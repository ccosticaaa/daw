﻿namespace DAWApp.Mappers.Subscriptions
{
    public class UpdateSubscriptionMapper
    {
        public int UserId { get; set; }
        public int TopicId { get; set; }
    }
}