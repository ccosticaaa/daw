﻿namespace DAWApp.Mappers.Subscriptions
{
    public class CreateSubscriptionMapper
    {
        public int UserId { get; set; }
        public int TopicId { get; set; }
    }
}