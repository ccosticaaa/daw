﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DAWApp.App;
using DAWApp.Mappers.ReadNews;
using DAWApp.Models;
using DAWApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DAWApp.Controllers
{   
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ReportsController : ControllerBase
    {
        private IReportsService _reportsService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public ReportsController(
            IReportsService reportsService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
        )
        {
            _reportsService = reportsService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            
        }
        
        
        
        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            try
            {

                IEnumerable<Task<Object>> allTasks = new []
                {
                    _reportsService.GetMostClickedNews(),
                    _reportsService.GetMostSubscribedNews()
                };
                Object[] allResults = await Task.WhenAll(allTasks);

                return Ok(allResults);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }

        }
    }
}