﻿﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
 using System.Linq;
 using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
 using DAWApp.App;
 using DAWApp.Mappers.Subscriptions;
 using DAWApp.Models;
 using DAWApp.Services;
 using Microsoft.AspNetCore.Authorization;

namespace DAWApp.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class SubscriptionsController : ControllerBase
    {
        private ISubscriptionService _subscriptionService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public SubscriptionsController(
            ISubscriptionService subscriptionService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
            )
        {
            _subscriptionService = subscriptionService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            
        }

        [HttpPost]
        public IActionResult Create(
            [FromBody] CreateSubscriptionMapper model
        )
        {
            // map model to entity
            var subscription = _mapper.Map<Subscription>(model);

            try
            {
                // create subscription
                subscription = _subscriptionService.Create(subscription);
                return Ok(subscription);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpGet]
        [Authorize(Roles = Role.AdminOrGuest)]
        public IActionResult GetAll()
        {
            var subscriptions = _subscriptionService.GetAll();
            var model = _mapper.Map<IList<SubscriptionMapper>>(subscriptions);
            return Ok(model);
        }
        
//        [HttpGet("app")]
//        [Authorize(Roles = Role.User)]
//        public IActionResult GetAllWithNews()
//        {
//            var subscriptions = _subscriptionService.GetAllWithNews();
//            var model = _mapper.Map<IList<SubscriptionMapper>>(subscriptions);
//            return Ok(model);
//        }
        
//        [HttpGet("{id}")]
//        public IActionResult GetById(int id)
//        {
//            try
//            {
//                // get subscription by id
//                var subscription = _subscriptionService.GetById(id);
//                var model = _mapper.Map<SubscriptionMapper>(subscription);
//                return Ok(model);
//            }
//            catch (AppException ex)
//            {
//                // return error message if there was an exception
//                return BadRequest(new { message = ex.Message });
//            }
//        }
        
        [HttpPut]
        public IActionResult CreateOrDelete([FromBody]CreateSubscriptionMapper model)
        {
            // map model to entity and set id
            var subscription = _mapper.Map<Subscription>(model);

            try
            {
                // update user 
                _subscriptionService.CreateOrDelete(subscription);
                return Ok(subscription);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                // delete subscription
                _subscriptionService.Delete(id);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }

        }
     
    }
}
