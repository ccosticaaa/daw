﻿﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
 using System.Linq;
 using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
 using DAWApp.App;
 using DAWApp.Mappers.News;
 using DAWApp.Models;
 using DAWApp.Services;
 using Microsoft.AspNetCore.Authorization;

namespace DAWApp.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class NewsController : ControllerBase
    {
        private INewsService _newsService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NewsController(
            INewsService newsService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
            )
        {
            _newsService = newsService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            
        }

        [HttpPost]
        public IActionResult Create(
            [FromBody] CreateNewsMapper mapper
        )
        {
            var news = _mapper.Map<News>(mapper);

            try
            {
                // create news
                _newsService.Create(news);
                return Ok(news);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpGet]
        [Authorize(Roles = Role.AdminOrGuest)]
        public IActionResult GetAll()
        {
            var news = _newsService.GetAll();
            var model = _mapper.Map<IList<NewsMapper>>(news);
            return Ok(model);
        }
        
        [HttpGet("topic/{id}")]
        public IActionResult GetByTopicId(int id)
        {
            try
            {
                // get news by id 
                var news = _newsService.GetByTopicId(id);
                var model = _mapper.Map<NewsMapper>(news);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpGet("app")]
        [Authorize(Roles = Role.User)]
        public IActionResult GetAllGroupedByTopicId()
        {
            try
            {
                var news = _newsService.GetAll().GroupBy(x => x.TopicId);
                
                var model = _mapper.Map<NewsMapper>(news);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                // get news by id 
                var news = _newsService.GetById(id);
                var model = _mapper.Map<NewsMapper>(news);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]UpdateNewsMapper mapper)
        {
            var news = _mapper.Map<News>(mapper);
            
            try
            {
                _newsService.Update(news);
                return Ok(news);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                // delete news 
                _newsService.Delete(id);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }

        }
     
    }
}
