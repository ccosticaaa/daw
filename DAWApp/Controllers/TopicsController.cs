﻿﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
 using System.Linq;
 using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
 using DAWApp.App;
 using DAWApp.Mappers.Topics;
 using DAWApp.Models;
 using DAWApp.Services;
 using Microsoft.AspNetCore.Authorization;

namespace DAWApp.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TopicsController : ControllerBase
    {
        private ITopicService _topicService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public TopicsController(
            ITopicService topicService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
            )
        {
            _topicService = topicService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            
        }

        [HttpPost]
        public IActionResult Create(
            [FromBody] CreateTopicMapper model
        )
        {
            // map model to entity
            var topic = _mapper.Map<Topic>(model);

            try
            {
                // create topic
                topic = _topicService.Create(topic);
                return Ok(topic);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpGet]
        [Authorize(Roles = Role.AdminOrGuest)]
        public IActionResult GetAll()
        {
            var topics = _topicService.GetAll();
            var model = _mapper.Map<IList<TopicMapper>>(topics);
            return Ok(model);
        }
        
        [HttpGet("app/{userId}")]
        [Authorize(Roles = Role.User)]
        public IActionResult GetTopicsByUserSubscription(int userId)
        {
            var topics = _topicService.GetTopicsByUserSubscription(userId);
            var model = _mapper.Map<IList<TopicMapper>>(topics);
            return Ok(model);
        }
        
        [HttpGet("customize/{userId}")]
        [Authorize(Roles = Role.User)]
        public IActionResult GetAllWithSubscriptions(int userId)
        {
            var topics = _topicService.GetAllWithSubscriptionsByUser(userId);
            var model = _mapper.Map<IList<TopicMapper>>(topics);
            return Ok(model);
        }
        
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                // get topic by id
                var topic = _topicService.GetById(id);
                var model = _mapper.Map<TopicMapper>(topic);
                return Ok(model);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]UpdateTopicMapper mapper)
        {
            // map model to entity and set id
            var topic = _mapper.Map<Topic>(mapper);
            topic.Id = id;

            try
            {
                // update user 
                _topicService.Update(topic);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                // delete topic
                _topicService.Delete(id);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }

        }
     
    }
}
