﻿using AutoMapper;
using DAWApp.App;
using DAWApp.Mappers.ReadNews;
using DAWApp.Models;
using DAWApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DAWApp.Controllers
{   
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ReadNewsController : ControllerBase
    {
        private IReadNewsService _readNewsService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public ReadNewsController(
            IReadNewsService newsService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
        )
        {
            _readNewsService = newsService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            
        }
        
        [HttpPost]
        public IActionResult Create(
            [FromBody] CreateReadNewsMapper mapper
        )
        {
            var readNews = _mapper.Map<ReadNews>(mapper);

            try
            {
                _readNewsService.Create(readNews);
                return Ok(readNews);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                // delete news 
                _readNewsService.Delete(id);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }

        }
    }
}