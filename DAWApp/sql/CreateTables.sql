﻿drop table Users;
drop table Topics;
drop table News;
drop table Subscriptions;
drop table ReadNews;

create table Users
(
	id int identity,
	email varchar(255),
	name varchar(255),
	role varchar(255),
	passwordHash varbinary(max),
	passwordSalt varbinary(max)
);

create table Topics
(
    id int identity
        constraint Topics_pk
            primary key nonclustered,
    name varchar(255)
);

create table News
(
	id int identity
		constraint News_pk
			primary key nonclustered,
	name varchar(255),
	body text,
	topicId int
);

create table Subscriptions
(
	id int identity
		constraint Subscriptions_pk
			primary key nonclustered,
	userId int,
	topicId int
);

create table ReadNews
(
	id int identity
		constraint ReadNews_pk
			primary key nonclustered,
	userId int,
	newsId int
);