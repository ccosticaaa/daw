﻿namespace DAWApp.App
{
    public class Helpers
    {
        public static bool isValidEmail(string email)
        {
            try {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch {
                return false;
            }
        }
    }
}