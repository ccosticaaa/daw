﻿using AutoMapper;
using DAWApp.Mappers.News;
using DAWApp.Mappers.ReadNews;
using DAWApp.Mappers.Reports;
using DAWApp.Mappers.Subscriptions;
using DAWApp.Mappers.Topics;
using DAWApp.Models;
using DAWApp.Mappers.Users;

namespace DAWApp.App
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserMapper>();
            CreateMap<RegisterMapper, User>();
            CreateMap<UpdateMapper, User>();

            CreateMap<Topic, TopicMapper>(); // this is not retarded 
            CreateMap<CreateTopicMapper, Topic>();
            CreateMap<UpdateTopicMapper, Topic>();
            
            CreateMap<News, NewsMapper>(); // this is not retarded 
            CreateMap<CreateNewsMapper, News>();
            CreateMap<UpdateNewsMapper, News>();
            
            CreateMap<ReadNews, ReadNewsMapper>(); // this is not retarded 
            CreateMap<CreateReadNewsMapper, ReadNews>();

            CreateMap<Subscription, SubscriptionMapper>();
            CreateMap<CreateSubscriptionMapper, Subscription>();
            CreateMap<UpdateSubscriptionMapper, Subscription>();
        }
    }
}