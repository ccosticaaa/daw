﻿using Microsoft.EntityFrameworkCore;
using DAWApp.Models;

namespace DAWApp.App
{
    public class Database : DbContext
    {
        public Database(DbContextOptions<Database> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<ReadNews> ReadNews { get; set; }
    }
}